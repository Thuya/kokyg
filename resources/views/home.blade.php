@extends('layouts/storeview')
	@section('content')
	<div class="container">
		<div class="shadow-box shadow mb-5 bg-white rounded">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			  	<div class="carousel-inner">
			    	<div class="carousel-item active">
			      		<img class="d-block w-100" src="../images/macbook.jpg" alt="First Slide">
			    	</div>
			    	<div class="carousel-item">
			      		<img class="d-block w-100" src="../images/iphone.jpg" alt="Second Slide">
			    	</div>
			  	</div>
			  	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				    <span class="fas fa-angle-left previous-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
			  	</a>
			  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    	<span class="fas fa-angle-right next-icon" aria-hidden="true"></span>
			    	<span class="sr-only">Next</span>
			  	</a>
			</div>
		</div>
	</div>
	<div class="container mb-4">
		<h2 class="text-left text-muted">Featured</h2>
		<div class="row">
			<div class="col-md-3 col-6">
				<div class="card mb-2 d-inline-block">
				    <img class="card-img-top" src="https://image.shutterstock.com/image-vector/smart-phone-vector-drawing-isolated-260nw-674250016.jpg" alt="Card image cap">
				    <div class="card-body">
				      	<h5 class="card-title">Samsung</h5>
				      	<p class="card-text font-small">
				      		Intel Core 2 Duo processor Powered by an Intel Core 2 Duo processor at speeds up to 2.1..
				      	</p>
				      	<p class="font-small">$602.00</p>
				      	<p class="font-small">Ex Tax: $500.00</p>
				    </div>
				    <div class="card-footer p-0">
				      	<div class="btn-group d-flex" role="group">
				      	  	<button type="button" class="btn btn-secondary flex-fill rounded-0">
				      	  		<i class="fas fa-shopping-cart"></i>
				      	  		<span>Shopping Cart</span>
				      	  	</button>
				      	  	<button type="button" title="add to whishlist" class="btn btn-secondary flex-fill">
				      	  		<i class="fas fa-heart"></i>
				      	  	</button>
				      	  	<button type="button" title="detail" class="btn btn-secondary flex-fill rounded-0 ml-0">
				      	  		<i class="fas fa-info"></i>
				      	  	</button>
				      	</div>
				    </div>
				</div>
			</div>
			<div class="col-md-3 col-6">
				<div class="card mb-2 d-inline-block">
				    <img class="card-img-top" src="https://image.shutterstock.com/image-vector/smart-phone-vector-drawing-isolated-260nw-674250016.jpg" alt="Card image cap">
				    <div class="card-body">
				      	<h5 class="card-title">Samsung</h5>
				      	<p class="card-text font-small">
				      		Intel Core 2 Duo processor Powered by an Intel Core 2 Duo processor at speeds up to 2.1..
				      	</p>
				      	<p class="font-small">$602.00</p>
				      	<p class="font-small">Ex Tax: $500.00</p>
				    </div>
				    <div class="card-footer p-0">
				      	<div class="btn-group d-flex" role="group">
				      	  	<button type="button" class="btn btn-secondary flex-fill rounded-0">
				      	  		<i class="fas fa-shopping-cart"></i>
				      	  		<span>Shopping Cart</span>
				      	  	</button>
				      	  	<button type="button" title="add to whishlist" class="btn btn-secondary flex-fill">
				      	  		<i class="fas fa-heart"></i>
				      	  	</button>
				      	  	<button type="button" title="detail" class="btn btn-secondary flex-fill rounded-0 ml-0">
				      	  		<i class="fas fa-info"></i>
				      	  	</button>
				      	</div>
				    </div>
				</div>
			</div>
			<div class="col-md-3 col-6">
				<div class="card mb-2 d-inline-block">
				    <img class="card-img-top" src="https://image.shutterstock.com/image-vector/smart-phone-vector-drawing-isolated-260nw-674250016.jpg" alt="Card image cap">
				    <div class="card-body">
				      	<h5 class="card-title">Samsung</h5>
				      	<p class="card-text font-small">
				      		Intel Core 2 Duo processor Powered by an Intel Core 2 Duo processor at speeds up to 2.1..
				      	</p>
				      	<p class="font-small">$602.00</p>
				      	<p class="font-small">Ex Tax: $500.00</p>
				    </div>
				    <div class="card-footer p-0">
				      	<div class="btn-group d-flex" role="group">
				      	  	<button type="button" class="btn btn-secondary flex-fill rounded-0">
				      	  		<i class="fas fa-shopping-cart"></i>
				      	  		<span>Shopping Cart</span>
				      	  	</button>
				      	  	<button type="button" title="add to whishlist" class="btn btn-secondary flex-fill">
				      	  		<i class="fas fa-heart"></i>
				      	  	</button>
				      	  	<button type="button" title="detail" class="btn btn-secondary flex-fill rounded-0 ml-0">
				      	  		<i class="fas fa-info"></i>
				      	  	</button>
				      	</div>
				    </div>
				</div>
			</div>
			<div class="col-md-3 col-6">
				<div class="card mb-2 d-inline-block">
				    <img class="card-img-top" src="https://image.shutterstock.com/image-vector/smart-phone-vector-drawing-isolated-260nw-674250016.jpg" alt="Card image cap">
				    <div class="card-body">
				      	<h5 class="card-title">MacBook</h5>
				      	<p class="card-text font-small">
				      		Intel Core 2 Duo processor Powered by an Intel Core 2 Duo processor at speeds up to 2.1..
				      	</p>
				      	<p class="font-small">$602.00</p>
				      	<p class="font-small">Ex Tax: $500.00</p>
				    </div>
				    <div class="card-footer p-0">
				      	<div class="btn-group d-flex" role="group">
				      	  	<button type="button" class="btn btn-secondary flex-fill rounded-0">
				      	  		<i class="fas fa-shopping-cart"></i>
				      	  		<span>Shopping Cart</span>
				      	  	</button>
				      	  	<button type="button" title="add to whishlist" class="btn btn-secondary flex-fill">
				      	  		<i class="fas fa-heart"></i>
				      	  	</button>
				      	  	<button type="button" title="detail" class="btn btn-secondary flex-fill rounded-0 ml-0">
				      	  		<i class="fas fa-info"></i>
				      	  	</button>
				      	</div>
				    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
	   <section class="customer-logos slider shadow mb-5 bg-white rounded">
	      <div class="slide"><img src="https://image.freepik.com/free-vector/luxury-letter-e-logo-design_1017-8903.jpg"></div>
	      <div class="slide"><img src="http://www.webcoderskull.com/img/logo.png"></div>
	      <div class="slide"><img src="https://image.freepik.com/free-vector/3d-box-logo_1103-876.jpg"></div>
	      <div class="slide"><img src="https://image.freepik.com/free-vector/blue-tech-logo_1103-822.jpg"></div>
	      <div class="slide"><img src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
	      <div class="slide"><img src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
	      <div class="slide"><img src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
	      <div class="slide"><img src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
	      <div class="slide"><img src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg"></div>
	   </section>
	</div>
	<footer class="text-center p-3">
		<p class="mb-0">Powered By KygKyi Phone © 2018</p>
	</footer>
	@endsection
	@section('scripts')
	<script>
		$(document).ready(function(){
		    $('.customer-logos').slick({
		        slidesToShow: 6,
		        slidesToScroll: 1,
		        autoplay: true,
		        autoplaySpeed: 1500,
		        arrows: false,
		        dots: false,
		        pauseOnHover: false,
		        responsive: [{
		            breakpoint: 768,
		            settings: {
		                slidesToShow: 4
		            }
		        }, {
		            breakpoint: 520,
		            settings: {
		                slidesToShow: 3
		            }
		        }]
		    });
		});
	</script>
	@endsection
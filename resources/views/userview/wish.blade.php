@extends('layouts.storeview')
	@section('content')
	<div class="container">
		<div class="card">
			<div class="card-body">
				<table id="cart" class="table table-hover">
			        <thead>
			        <tr>
			            <th>Product</th>
			            <th>Price</th>
			            <th>Quantity</th>
			            <th></th>
			        </tr>
			        </thead>
			        <tbody>
			        @if(session('wish'))
			            @foreach(session('wish') as $id => $details)
			                <tr>
			                    <td data-th="Product">
			                        <div class="row">
			                            <div class="col-3 hidden-xs"><img src="{{ $details['photo'] }}" width="100" height="110"/></div>
			                            <div class="col-9">
			                                <h4 class="mb-0 pt-5">{{ $details['name'] }}</h4>
			                            </div>
			                        </div>
			                    </td>
			                    <td data-th="Price">${{ $details['price'] }}</td>
			                    <td data-th="Quantity">
			                        {{ $details['totalqty'] }}
			                    </td>
			                    <td class="actions" data-th="">
			                        <a href="{{ url('/wish/remove/', $id) }}" class="btn btn-danger btn-sm remove-from-cart"><i class="fas fa-trash-alt"></i></a>
			                    </td>
			                </tr>
			            @endforeach
			        @endif

			        </tbody>
			    </table>
			    <a href="{{ route('products.all') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a>
			</div>
		</div>
	</div>
	@endsection
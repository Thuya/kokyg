@extends('layouts.storeview')
	@section('content')
	<div class="container mb-4 mt-4">
		@if(session()->has('success'))
			<div class="alert alert-success" role="alert">
				{{ session('success') }}
			</div>
		@endif
		
		@if(isset($category))
		<h2 class="text-left text-muted">{{ $category->name }}</h2>
		@else 
		<h2 class="text-left text-muted">All Products</h2>
		@endif
		<div class="row">

			@forelse( $phones as $phone )
			<div class="col-md-3 col-6">
				<div class="card mb-5 pt-2 d-inline-block">
				    <img class="card-img-top" height="278" src="{{ $phone->photo }}" alt="Card image cap">
				    <div class="card-body">
				      	<h5 class="card-title">{{ $phone->title }}</h5>
				      	<p class="card-text font-small">
				      		Intel Core 2 Duo processor Powered by an Intel Core 2 Duo processor at speeds up to 2.1..
				      	</p>
				      	<p class="font-small">$602.00</p>
				      	<p class="font-small">Ex Tax: $500.00</p>
				    </div>
				    <div class="card-footer p-0">
				      	<div class="btn-group d-flex" role="group">
				      	  	<button data-id="{{ $phone->id }}" role="button" class="btn text-white btn-secondary flex-fill rounded-0 addCart">
				      	  		<i class="fas fa-shopping-cart"></i>
				      	  		<span>Shopping Cart</span>
				      	  	</button>
				      	  	<?php $wishlist = session()->get('wish'); ?>
				      	  	
							@if( isset($wishlist[$phone->id]) )
	      	  				<button data-id="{{ $phone->id }}" title="add to whishlist" class="btn text-white btn-secondary flex-fill removeWish">
	      	  	      	  		<i class="fas fa-heart text-warning"></i>
	      	  	      	  	</button>
				      	  	@else
				      	  	<button data-id="{{ $phone->id }}" title="add to whishlist" class="btn text-white btn-secondary flex-fill addWish">
				      	  		<i class="fas fa-heart"></i>
				      	  	</button>
				      	  	@endif

				      	  	<a href="{{ route('products.detail', $phone->id) }}" title="detail" class="btn text-white btn-secondary flex-fill rounded-0 ml-0">
				      	  		<i class="fas fa-info"></i>
				      	  	</a>
				      	</div>
				    </div>
				</div>
			</div>
			@empty
			<h1>No Product Found!</h1>
			@endforelse
		</div>
	</div>
	@endsection

	@section('scripts')
	<script>
		$(document).ready(function() {
			$('.addCart').click( function(e) {
				e.preventDefault();
				var id = $(this).attr('data-id');
				$.ajax({
				   url: '{{ url('/add-to-cart/') }}',
				   method: "post",
				   data: {_token: '{{ csrf_token() }}', id: id},
				   success: function (response) {
				       window.location.reload();
				   }
				});
			});

			$('.addWish').click( function(e) {
				e.preventDefault();
				var id = $(this).attr('data-id');
				$.ajax({
				   url: '{{ url('/wish/add') }}',
				   method: "post",
				   data: {_token: '{{ csrf_token() }}', id: id},
				   success: function (response) {
				       window.location.reload();
				   }
				});
			});

			$('.removeWish').click( function(e) {
				e.preventDefault();
				var id = $(this).attr('data-id');
				$.ajax({
				   url: '{{ url('/wish/remove/') }}',
				   method: "post",
				   data: {_token: '{{ csrf_token() }}', id: id},
				   success: function (response) {
				       window.location.reload();
				   }
				});
			});
		})
	</script>
	@endsection
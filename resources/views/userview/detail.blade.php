@extends('layouts.storeview')
	@section('content')
	<div class="container">
		<div class="card">
			<div class="card-body">
				<div class="container">
					<div class="row border-bottom pb-5">
						<div class="col-md-6">
							<img src="{{ $product->photo }}" class="img-fluid">
						</div>
						<div class="col-md-6 mt-5 border-left">
							<h3 class="mb-4">{{ $product->name }}</h3>
							<h3 class="mb-4"><strong>$ {{ $product->price }}</strong></h3>
							<?php $cartt = session()->get('cart'); ?>
				      	  	
							@if( isset($cartt[$product->id]) )
							<button class="btn btn-dark btn-lg" title="This product is already added to cart" disabled>Add to Cart</button>
							@else
							<a href="{{ url('/add-to-cart/' . $product->id) }}" class="btn btn-dark btn-lg">Add to Cart</a>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 p-5">
							<h3>Detail Info</h3>
							<ul class="p-3">
								<li>{{ $product->cpu }} CPU</li>
								<li>{{ $product->ram }} RAM</li>
								<li>Memory {{ $product->storage }}</li>
								<li>{{ $product->resolution }} Full HD Resolution</li>
								<li>{{ $product->camera }}</li>
								<li>{{ $product->operating_system }}</li>
								<li>{{ $product->description }}</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection
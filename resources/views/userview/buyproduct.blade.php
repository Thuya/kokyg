@extends('layouts.storeview')
	@section('content')
	
	<div class="container">
		<div class="card pt-4 pb-4">
			<div class="card-body m-auto">
				<h3>Thank You for buying products {{ $buy->user->name }}</h3>
				<p>
					You bought
					 @forelse ( $buys as $buyss )
						{{ $buyss->item_name }}
						@if(! $loop->last )
						,
						@endif
					@empty
						No Product!
					@endforelse
				</p>
				<p class="mb-4">Total Amount is $ {{ $buy->total_amount }}.</p>
				<strong>Confirmation email will be send to your gmail account.</strong><br>
				<strong>Your Product will be deliver soon!</strong><br>
				<a href="/" class="mt-4 btn btn-success">< Go Back Home</a>
			</div>
		</div>
	</div>
	
	@endsection
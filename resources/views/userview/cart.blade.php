@extends('layouts.storeview')
	@section('content')
	<div class="container">
		@if(session()->has('success'))
			<div class="alert alert-success mt-4" role="alert">
				{{ session('success') }}
			</div>
		@endif
		<div class="card">
			<div class="card-body">
				<table id="cart" class="table table-hover table-condensed table-responsive">
			        <thead>
			        <tr>
			            <th style="width:50%">Product</th>
			            <th style="width:10%">Price</th>
			            <th style="width:8%">Quantity</th>
			            <th style="width:22%" class="text-center">Subtotal</th>
			            <th style="width:10%"></th>
			        </tr>
			        </thead>
			        <tbody>

			        <?php $total = 0 ?>

			        @if(session('cart'))
			            @foreach(session('cart') as $id => $details)

			                <?php $total += $details['price'] * $details['quantity'] ?>

			                <tr class="border-bottom">
			                    <td data-th="Product">
			                        <div class="row">
			                            <div class="col-3 hidden-xs"><img src="{{ $details['photo'] }}" width="100" height="130"/></div>
			                            <div class="col-9">
			                                <h4 class="mb-0 p-5">{{ $details['name'] }}</h4>
			                            </div>
			                        </div>
			                    </td>
			                    <td data-th="Price">${{ $details['price'] }}</td>
			                    <td data-th="Quantity">
			                        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
			                    </td>
			                    <td data-th="Subtotal" class="text-center">${{ $details['price'] * $details['quantity'] }}</td>
			                    <td class="actions" data-th="">
			                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fas fa-redo"></i></button>
			                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fas fa-trash-alt"></i></button>
			                    </td>
			                </tr>
			            @endforeach
			        @endif

			        </tbody>
			        <tfoot>
			        <tr>
			            <td><a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
			            <td colspan="2" class="hidden-xs"></td>
			            <td class="hidden-xs text-center">
			            	<strong>Total : ${{ $total }}</strong>
			            </td>
			            <td>
			            	@if(session('cart'))
			            	<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
			            	  	CheckOut
			            	</button>
			            	@endif
			            </td>
			        </tr>
			        </tfoot>
			    </table>
			    @if(! session('cart'))
			    <p>You should need to buy product firstly!</p>
			    @endif
			    <div class="modal fade align-middle" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			      	<div class="modal-dialog" role="document">
			        	<div class="modal-content">
			          		<div class="modal-header">
			            		<h5 class="modal-title" id="exampleModalLabel">Checkout Products</h5>
			            		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			              			<span aria-hidden="true">&times;</span>
			            		</button>
			          		</div>
			          		<form action="{{ route('checkout') }}" class="checkoutForm" method="POST">
			          			{{ csrf_field() }}
				          		<div class="modal-body">
						            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
						            <input type="hidden" name="order_month" value="{{ $month }}">
						            <div class="form-group">
						            	<input type="phone" name="phone" class="form-control" placeholder="Type Your Phone Number" required>
						            </div>
						            <div class="form-group">
						            	<textarea name="address" class="form-control" cols="30" rows="5" placeholder="Type Your Address" required></textarea>
						            </div>
						            <div class="form-group">
						            	<input name="order_date" value="{{ date('Y-m-d') }}" class="form-control" readonly>
						            </div>
					            	<div class="form-group">
					            		<select name="payment" class="form-control js-example-basic-single">
			   								<option value="visa">Visa</option>
			   								<option value="paypal">Paypal</option>
			   								<option value="stripe">Stripe</option>
			   								<option value="master">Master</option>
			   							</select>
					            	</div>
		   							<div class="form-group">
		   								<input type="text" placeholder="Payment Account" class="form-control" required>
		   							</div>
		   							<div class="form-group">
		   								<label>Total Products</label>
		   								<input type="number" name="total_quantity" value="{{ count(session('cart')) }}"  class="form-control" readonly>
		   								@if ($errors->has('total_quantity'))
							   				<div class="alert alert-danger mt-2" role="alert">
							   					{{ $errors->first('total_quantity') }}
							   				</div>
						   				@endif
		   							</div>
		   							<div class="input-group mb-3">
		   							  	<div class="input-group-prepend">
		   							    	<span class="input-group-text" id="inputGroupFileAddon01">$</span>
		   							  	</div>
		   							  	<div class="custom-file">
		   							    	<input type="number" name="total_amount" value="{{ $total }}"  class="form-control" readonly>
		   							  	</div>
		   							</div>
					   				@if ($errors->has('total_amount'))
						   				<div class="alert alert-danger mt-2" role="alert">
						   					{{ $errors->first('total_amount') }}
						   				</div>
					   				@endif
		   							<div class="form-group">
		   								<textarea name="note" class="form-control d-flex" cols="30" rows="3">You will be buy @if(session('cart')) @foreach(session('cart') as $id => $details){{ $details['name'] }} @if(! $loop->last),@endif @endforeach @else No Cat! @endif
		   								</textarea>
						   				@if ($errors->has('note'))
							   				<div class="alert alert-danger mt-2" role="alert">
							   					{{ $errors->first('note') }}
							   				</div>
						   				@endif
		   							</div>
						        </div>
				          		<div class="modal-footer">
				            		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				            		<button type="button" class="btn btn-success paid">Paid</button>
				          		</div>
			          		</form>
			        	</div>
			      	</div>
			    </div>
			</div>
		</div>
	</div>
	@endsection

	@section('scripts')	
	<script type="text/javascript">

	    $(".update-cart").click(function (e) {
	       e.preventDefault();

	       var ele = $(this);

	        $.ajax({
	           url: '{{ url('update-cart') }}',
	           method: "patch",
	           data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
	           success: function (response) {
	               console.log('success');
	           }
	        });
	    });

	    $(".remove-from-cart").click(function (e) {
	        e.preventDefault();

	        var ele = $(this);

	        if(confirm("Are you sure")) {
	            $.ajax({
	                url: '{{ url('remove-from-cart') }}',
	                method: "DELETE",
	                data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
	                success: function (response) {
	                    window.location.reload();
	                }
	            });
	        }
	    });

	    $(".paid").click(function (e) {
	    	e.preventDefault();
	    	$(".checkoutForm").submit();
	    });
	</script>
	@endsection
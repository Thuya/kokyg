@extends('layouts.storeview')
	@section('content')
		<div class="container">
			<div class="card">
				<div class="card-header"><h3>My History</h3></div>
				<div class="card-body">
					<table class="table">
					  	<thead class="thead-dark">
						    <tr>
						      <th scope="col">No</th>
						      <th scope="col">Names</th>
						      <th scope="col">Total Quantity</th>
						      <th scope="col">Total Amount</th>
						    </tr>
					  	</thead>
					  	<tbody>
					  		@forelse($orders as $order)
						    <tr>
						      <th scope="row">{{ $order->id }}</th>
						      <td>
						      	@foreach($order->orderdetails as $name)
						      		{{ $name->item_name }}
						      		@if(! $loop->last )
						      		,
						      		@endif
						      	@endforeach
						      </td>
						      <td>{{ $order->total_quantity }}</td>
						      <td>$ {{ $order->total_amount }}</td>
						    </tr>
						    @empty
								<tr class="text-center">
									<td colspan="4">Not history yet!</td>
								</tr>
							@endforelse
					  	</tbody>
					</table>
					
				</div>
			</div>
		</div>
	@endsection
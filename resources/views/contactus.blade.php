@extends('layouts/storeview')
	@section('content')
	<div class="container">
		<div class="shadow-box shadow mb-5 bg-white rounded p-4">
			<p>
				(1) KygLyi - 36 street <br>
				No.145 - 230, 36 street(middle block) <br>
				Kyouktada township, Yangon <br>
				Tel: 09-83024822,09-123456789 
			</p>
			<p>
				(2) KygLyi - U Chit Mg Road <br>
				No.15 - 20, U Chit Mg Road <br>
				Bahan township, Yangon <br>
				Tel: 09-3930930,09-2931913 
			</p>
			<p>
				(3) KygLyi - La Thar <br>
				No.5 - 23, 17 street(middle block) <br>
				La Thar township, Yangon <br>
				Tel: 09-892842234,09-89183131 
			</p>
			<p>
				(4) KygLyi - 30 street <br>
				No.147 - 189, 30 street(middle block) <br>
				Kyouktada township, Yangon <br>
				Tel: 09-89280324,09-987654321 
			</p>
			<p>
				(5) KygLyi - 136 street <br>
				No.83 - 21, 136 street <br>
				Tarmwe township, Yangon <br>
				Tel: 09-0382084032,09-82308204 
			</p>
			<p>
				(6) KygLyi - 30 street <br>
				No.105 - 223, 30 street(middle block) <br>
				Mandalay township, Madalay <br>
				Tel: 09-38294292,09-99332432 
			</p>
			<p>
				(7) KygLyi - Bu tar yon Road <br>
				No.22 - 89, Bu tar yon Road <br>
				Mandalay township, Mandalay <br>
				Tel: 09-38208402,09-873293277 
			</p>
		</div>
	</div>
	@endsection
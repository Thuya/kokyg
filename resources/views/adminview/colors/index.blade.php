@extends('layouts/adminview')
	@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
	<a href="{{ route('colors.create') }}" class="btn btn-success mt-4">Create</a>
	<table class="table mt-3">
	  	<thead class="thead-dark">
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Name</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $colors as $color )
		    	<tr>
		    		<th scope="row">{{ $color->id }}</th>
		    		<td>{{ $color->name }}</td>
		    		<td>
		    			<a href="{{ route('colors.edit', $color->id) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
		    			<a href="{{ route('colors.delete', $color->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
		    		</td>
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="3"><h3>No Color!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>
	@endsection
@extends('layouts/adminview')
	@section('content')
	<div class="card position-relative mt-5">
	  	<div class="card-header">
	  		<div class="p-3 position-absolute rounded" style="top: -24px; margin-bottom: -24px; background: #000;">
	  			<div class="pa-4 v-card theme--dark green elevation-10">
	  				<i aria-hidden="true" class="fas fa-lock" style="font-size: 40px; color: #fff;"></i>
	  			</div>
	  		</div>
	  		<h3 style="margin-left: 89px;">Product</h3>
	  	</div>
	  	<div class="card-body">
		   <form action="{{ route('products.store') }}" method="POST">
		   		{{ csrf_field() }}
		   		<div class="form-group">
		   			<label>Product Name</label>
		   			<input type="text" name="name" class="form-control" value="{{ old('name') }}">
		   				@if ($errors->has('name'))
			   				<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('name') }}
			   				</div>
		   				@endif
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>CPU</label>
		   					<input type="text" name="cpu" class="form-control">
		   					@if ($errors->has('cpu'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('cpu') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>RAM</label>
		   					<input type="text" name="ram" class="form-control">
		   					@if ($errors->has('ram'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('ram') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Resolution</label>
		   					<input type="text" name="resolution" class="form-control">
		   					@if ($errors->has('resolution'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('resolution') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Screen Display</label>
		   					<input type="text" name="screen_display" class="form-control">
		   					@if ($errors->has('screen_display'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('screen_display') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Storage</label>
		   					<input type="text" name="storage" class="form-control">
		   					@if ($errors->has('storage'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('storage') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Operating System</label>
		   					<input type="text" name="operating_system" class="form-control">
		   					@if ($errors->has('operating_system'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('operating_system') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Camera</label>
		   					<input type="text" name="camera" class="form-control">
		   					@if ($errors->has('camera'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('camera') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Price</label>
		   					<input type="number" name="price" class="form-control">
		   					@if ($errors->has('price'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('price') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Photo</label>
		   					<input type="text" name="photo" class="form-control">
		   					@if ($errors->has('photo'))
				   				<div class="alert alert-danger mt-2" role="alert">
				   					{{ $errors->first('photo') }}
				   				</div>
			   				@endif
		   				</div>
		   			</div>
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Category</label>
		   					<select name="category_id" class="form-control js-example-basic-single">
		   						<option>Choose Category</option>
		   						@forelse($categories as $category)
		   							<option value="{{ $category->id }}">{{ $category->name }}</option>
		   						@empty
		   							<option>No Category!</option>
		   						@endforelse
		   					</select>
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Color</label>
		   					<select name="color_id" class="form-control js-example-basic-single">
		   						<option>Choose Color</option>
		   						@forelse($colors as $color)
		   							<option value="{{ $color->id }}">{{ $color->name }}</option>
		   						@empty
		   							<option>No Color!</option>
		   						@endforelse
		   					</select>
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Brand</label>
		   					<select name="brand_id" class="form-control js-example-basic-single">
		   						<option>Choose Brand</option>
		   						@forelse($brands as $brand)
		   							<option value="{{ $brand->id }}">{{ $brand->name }}</option>
		   						@empty
		   							<option>No Brand!</option>
		   						@endforelse
		   					</select>
		   				</div>
		   			</div>
		   		</div>
		   		<div class="form-group">
		   			<label>Description</label>
		   			<textarea name="description" class="form-control" cols="30" rows="5"></textarea>
   					@if ($errors->has('description'))
		   				<div class="alert alert-danger mt-2" role="alert">
		   					{{ $errors->first('description') }}
		   				</div>
	   				@endif
		   		</div>
   				<div class="form-group">
   					<label>Total Quantity</label>
   					<input type="number" name="totalqty" class="form-control">
   					@if ($errors->has('totalqty'))
		   				<div class="alert alert-danger mt-2" role="alert">
		   					{{ $errors->first('totalqty') }}
		   				</div>
	   				@endif
   				</div>
		   		<button class="btn btn-success">Create</button>
		   </form>
	  	</div>
	</div>
	@endsection
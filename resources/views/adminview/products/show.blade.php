@extends('layouts/adminview')
	@section('content')
	<div class="card position-relative mt-3">
	  	<div class="card-body">
			<div class="container">
				<a href="{{ route('products') }}" class="btn btn-dark text-white" title="back to products page"><i class="fas fa-arrow-left"></i></a>
				<div class="row">
					<div class="col-md-6">
						<img src="{{ $product->photo }}" alt="{{ $product->name }}" class="w-100 img-fluid">
						<div class="bg-dark text-white">
							<h2>{{ $product->name }}</h2>
						</div>
					</div>
					<div class="col-md-6">
						<ul class="list-group list-group-flush pt-5 product-show">
							<li class="list-group-item"><h3>Detail Info</h3></li>
							<li class="list-group-item">CPU => <strong>{{ $product->cpu }}</strong></li>
							<li class="list-group-item">RAM => <strong>{{ $product->ram }}</strong></li>
							<li class="list-group-item position-relative">Color => <span class="position-absolute" style="background-color: {{ $product->color->name }}; width: 20px; height: 20px; top: 13px; margin-left: 7px;"></span></li>
							<li class="list-group-item">Category => <strong>{{ $product->category->name }}</strong></li>
							<li class="list-group-item">Brand => <strong>{{ $product->brand->name }}</strong></li>
							<li class="list-group-item">Resolution => <strong>{{ $product->resolution }}</strong></li>
							<li class="list-group-item">Screen Display => <strong>{{ $product->screen_display }}</strong></li>
							<li class="list-group-item">Storage => <strong>{{ $product->storage }}</strong></li>
							<li class="list-group-item">Camera => <strong>{{ $product->camera }}</strong></li>
							<li class="list-group-item">Price => <strong>{{ $product->price }}</strong></li>
							<li class="list-group-item">Total Quantity => <strong>{{ $product->totalqty }}</strong></li>
							<li class="list-group-item"><strong>{{ $product->description }}</strong></li>
						</ul>
					</div>
				</div>
			</div>
	  	</div>
	</div>
	@endsection
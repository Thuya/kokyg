@extends('layouts/adminview')
	@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
	<a href="{{ route('products.create') }}" class="btn btn-success">Create</a>
	<table class="table mt-5">
	  	<thead class="thead-dark">
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Name</th>
		      	<th scope="col">Color</th>
		      	<th scope="col">Category</th>
		      	<th scope="col">Brand</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $products as $product )
		    	<tr>
		    		<th scope="row">{{ $product->id }}</th>
		    		<td>{{ $product->name }}</td>
		    		<td class="position-relative"><span class="position-absolute" style="background-color: {{ $product->color->name }}; width: 20px; height: 20px; top: 15px;"></span></td>
		    		<td>{{ $product->category->name }}</td>
		    		<td>{{ $product->brand->name }}</td>
		    		<td>
		    			<a href="{{ route('products.show', $product->id) }}" title="detail of {{ $product->name }}" class="btn btn-info"><i class="fas fa-table"></i></a>
		    			<a href="{{ route('products.edit', $product->id) }}" title="edit" class="btn btn-warning"><i class="fas fa-edit"></i></a>
		    			<a href="{{ route('products.delete', $product->id) }}" title="delete" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
		    		</td>
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="3"><h3>No Products!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>
	@endsection
@extends('layouts/adminview')
	@section('content')
	<div class="card position-relative mt-5">
	  	<div class="card-header">
	  		<div class="p-3 position-absolute rounded" style="top: -24px; margin-bottom: -24px; background: #000;">
	  			<div class="pa-4 v-card theme--dark green elevation-10">
	  				<i aria-hidden="true" class="fas fa-lock" style="font-size: 40px; color: #fff;"></i>
	  			</div>
	  		</div>
	  		<h3 style="margin-left: 89px;">Update {{ $product->name }}</h3>
	  	</div>
	  	<div class="card-body">
		   <form action="{{ route('products.update', $product->id) }}" method="POST">
		   		{{ csrf_field() }}
		   		<input type="hidden" name="id" value="{{ $product->id }}">
		   		<div class="form-group">
		   			<label>Product Name</label>
		   			<input type="text" name="name" class="form-control" value="{{ $product->name }}">
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>CPU</label>
		   					<input type="text" name="cpu" class="form-control" value="{{ $product->cpu }}">
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>RAM</label>
		   					<input type="text" name="ram" class="form-control" value="{{ $product->ram }}">
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Resolution</label>
		   					<input type="text" name="resolution" value="{{ $product->resolution }}" class="form-control">
		   				</div>
		   			</div>
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Screen Display</label>
		   					<input type="text" name="screen_display" value="{{ $product->screen_display }}" class="form-control">
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Storage</label>
		   					<input type="text" name="storage" value="{{ $product->storage }}" class="form-control">
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Operating System</label>
		   					<input type="text" name="operating_system" value="{{ $product->operating_system }}" class="form-control">
		   				</div>
		   			</div>
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Camera</label>
		   					<input type="text" name="camera" value="{{ $product->camera }}" class="form-control">
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Price</label>
		   					<input type="number" name="price" value="{{ $product->price }}" class="form-control">
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Photo</label>
		   					<input type="text" name="photo" value="{{ $product->photo }}" class="form-control">
		   				</div>
		   			</div>
		   		</div>
		   		<div class="row">
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Category</label>
		   					<select name="category_id" class="form-control js-example-basic-single">
		   						<option>Choose Category</option>
		   						@forelse($categories as $category)
		   							<option 
									@if( $product->category->id == $category->id )
										selected="selected"
									@endif
		   							 value="{{ $category->id }}">{{ $category->name }}</option>
		   						@empty
		   							<option>No Category!</option>
		   						@endforelse
		   					</select>
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Color</label>
		   					<select name="color_id" class="form-control js-example-basic-single">
		   						<option>Choose Color</option>
		   						@forelse($colors as $color)
		   							<option 
		   							@if( $product->color->id == $color->id )
										selected="selected"
									@endif
		   							value="{{ $color->id }}">{{ $color->name }}</option>
		   						@empty
		   							<option>No Color!</option>
		   						@endforelse
		   					</select>
		   				</div>
		   			</div>
		   			<div class="col-md-4">
		   				<div class="form-group">
		   					<label>Brand</label>
		   					<select name="brand_id" class="form-control js-example-basic-single">
		   						<option>Choose Brand</option>
		   						@forelse($brands as $brand)
		   							<option
		   							@if( $product->brand->id == $brand->id )
		   								selected="selected"
		   							@endif
		   							value="{{ $brand->id }}">{{ $brand->name }}</option>
		   						@empty
		   							<option>No Brand!</option>
		   						@endforelse
		   					</select>
		   				</div>
		   			</div>
		   		</div>
		   		<div class="form-group">
		   			<label>Description</label>
		   			<textarea name="description" class="form-control" cols="30" rows="5">{{ $product->description }}</textarea>
		   		</div>
   				<div class="form-group">
   					<label>Total Quantity</label>
   					<input type="number" value="{{ $product->totalqty }}" name="totalqty" class="form-control">
   				</div>
		   			
		   		<button class="btn btn-warning">Update</button>
		   </form>
	  	</div>
	</div>
	@endsection
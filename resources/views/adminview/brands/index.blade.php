@extends('layouts/adminview')
	@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
	<a href="{{ route('brands.create') }}" class="btn btn-success">Create</a>
	<table class="table mt-5">
	  	<thead class="thead-dark">
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Name</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $brands as $brand )
		    	<tr>
		    		<th scope="row">{{ $brand->id }}</th>
		    		<td>{{ $brand->name }}</td>
		    		<td>
		    			<a href="{{ route('brands.edit', $brand->id) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
		    			<a href="{{ route('brands.delete', $brand->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
		    		</td>
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="3"><h3>No Color!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>
	@endsection
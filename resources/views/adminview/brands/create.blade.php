@extends('layouts/adminview')
	@section('content')
	<div class="card position-relative mt-5">
	  	<div class="card-header">
	  		<div class="p-3 position-absolute rounded" style="top: -24px; margin-bottom: -24px; background: #000;">
	  			<div class="pa-4 v-card theme--dark green elevation-10">
	  				<i aria-hidden="true" class="fab fa-apple" style="font-size: 40px; color: #fff;"></i>
	  			</div>
	  		</div>
	  		<h3 style="margin-left: 89px;">Brands</h3>
	  	</div>
	  	<div class="card-body">
		   <form action="{{ route('brands.store') }}" method="POST">
		   		{{ csrf_field() }}
		   		<div class="form-group">
		   			<label>Brand Name</label>
		   			<input type="text" name="name" class="form-control">
		   				@if ($errors->has('name'))
			   				<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('name') }}
			   				</div>
		   				@endif
		   		</div>
		   		<button class="btn btn-success">Create</button>
		   </form>
	  	</div>
	</div>
	@endsection
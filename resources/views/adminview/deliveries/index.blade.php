@extends('layouts/adminview')
	@section('content')
	<table class="table mt-5">
	  	<thead class="thead-dark">
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Customer Name</th>
		      	<th scope="col">Fees</th>
		      	<th scope="col">Phone</th>
		      	<th scope="col">Address</th>
		      	<th scope="col">status</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $deliveries as $delivery )
		    	<tr>
		    		<th scope="row">{{ $delivery->id }}</th>
		    		<td>{{ $delivery->contact_person }}</td>
		    		<td>{{ $delivery->fees }}</td>
		    		<td>{{ $delivery->phone }}</td>
		    		<td>{{ $delivery->address }}</td>
		    		<td>{{  $delivery->status }}</td>
		    		@if($delivery->status == 'completed')
		    		<td>
		    			<span class="badge badge-success">Success</span>
		    		</td>
		    		@else
		    		<td>
		    			<a href="{{ route('delivery.status', $delivery->id) }}" title="if product sent click click this" class="btn btn-info"><i class="text-white fas fa-truck"></i></a>
		    		</td>
		    		@endif
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="8"><h3>No Delivery!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>
	@endsection
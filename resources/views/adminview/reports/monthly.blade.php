<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>IT Devices</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<table class="table mt-5">
	  	<thead class="thead-dark">
		    <tr>
		      <th scope="col">No</th>
		      <th scope="col">User Name</th>
		      <th scope="col">Email</th>
		      <th scope="col">Product Name</th>
		      <th scope="col">Total Quantity</th>
		      <th scope="col">Total Amount</th>
		    </tr>
	  	</thead>
	  	<tbody>
	  		@forelse($reports as $report)
		    <tr>
		      <th scope="row">{{ $report->id }}</th>
		      <td>{{ $report->user->name }}</td>
		      <td>{{ $report->user->email }}</td>
		      <td>
		      	@foreach($report->orderdetails as $name)
		      		{{ $name->item_name }}
		      		@if(! $loop->last )
		      		,
		      		@endif
		      	@endforeach
		      </td>
		      <td>{{ $report->total_quantity }}</td>
		      <td>$ {{ $report->total_amount }}</td>
		    </tr>
		    @empty
				<tr class="text-center">
					<td>Not Report yet!</td>
				</tr>
			@endforelse
	  	</tbody>
	</table>

</body>
</html>
@extends('layouts/adminview')
	@section('content')
	@if(session()->has('success'))
		<div class="alert alert-success mt-2" role="alert">
			{{ session('success') }}
		</div>
	@endif
	<a href="{{ route('reports.all') }}" class="btn btn-danger">Generate Sale All</a>
	<a href="{{ route('reports.monthly') }}" class="btn btn-danger">Monthly Sale Report</a>
	<a href="{{ route('reports.daily') }}" class="btn btn-danger">Today Sale Report</a>
	<table class="table mt-5">
	  	<thead class="thead-dark">
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Name</th>
		      	<th scope="col">Color</th>
		      	<th scope="col">Category</th>
		      	<th scope="col">Brand</th>
		      	<th scope="col">Quantity</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $reports as $report )
		    	<tr>
		    		<th scope="row">{{ $report->id }}</th>
		    		<td>{{ $report->name }}</td>
		    		<td class="position-relative"><span class="position-absolute" style="background-color: {{ $report->color->name }}; width: 20px; height: 20px; top: 15px;"></span></td>
		    		<td>{{ $report->category->name }}</td>
		    		<td>{{ $report->brand->name }}</td>
		    		<td>{{ $report->totalqty }}</td>
		    		<td>
		    			<a href="{{ route('purchases', $report->id) }}" title="purchase product from distributors" class="btn btn-info"><i class="text-white fas fa-money-bill-alt"></i></a>
		    		</td>
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="3"><h3>No Products!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>

	<h1 class="mt-5 mb-2">Supplier Lists</h1>
	<hr>
	<a href="{{ route('suppliers.create') }}" class="btn btn-success mb-3">Create</a>
	<table class="table mb-4">
	  	<thead class="thead-dark">
		    <tr>
		      <th scope="col">No</th>
		      <th scope="col">User Name</th>
		      <th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
	  		@forelse($suppliers as $supplier)
		    <tr>
		      <th scope="row">{{ $supplier->id }}</th>
		      <td>{{ $supplier->name }}</td>
		      <td>
		      	<a href="{{ route('suppliers.edit', $supplier->id) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
		      	<a href="{{ route('suppliers.delete', $supplier->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
		      </td>
		    </tr>
		    @empty
				<tr class="text-center">
					<td>Not Supplier yet!</td>
				</tr>
			@endforelse
	  	</tbody>
	</table>
	@endsection
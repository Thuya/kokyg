@extends('layouts/adminview')
	@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
	<a href="{{ route('categories.create') }}" class="btn btn-success">Create</a>
	<table class="table mt-5">
	  	<thead class="thead-dark">
		    <tr>
		      	<th scope="col">No</th>
		      	<th scope="col">Name</th>
		      	<th scope="col">Actions</th>
		    </tr>
	  	</thead>
	  	<tbody>
		    @forelse( $categories as $category )
		    	<tr>
		    		<th scope="row">{{ $category->id }}</th>
		    		<td>{{ $category->name }}</td>
		    		<td>
		    			<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
		    			<a href="{{ route('categories.delete', $category->id) }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
		    		</td>
		    	</tr>
		    @empty
		    <tr>
		    	<td colspan="3"><h3>No Category!</h3></td>
		    </tr>
		    @endforelse
	  	</tbody>
	</table>
	@endsection
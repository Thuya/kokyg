@extends('layouts/adminview')
	@section('content')
	<div class="card position-relative mt-5">
	  	<div class="card-header">
	  		<div class="p-3 position-absolute rounded" style="top: -24px; margin-bottom: -24px; background: #000;">
	  			<div class="pa-4 v-card theme--dark green elevation-10">
	  				<i aria-hidden="true" class="fas fa-palette" style="font-size: 40px; color: #fff;"></i>
	  			</div>
	  		</div>
	  		<h3 style="margin-left: 89px;">Update {{ $category->name }}</h3>
	  	</div>
	  	<div class="card-body">
		   <form action="{{ route('categories.update', $category->id) }}" method="POST">
		   		{{ csrf_field() }}
		   		<input type="hidden" name="id" value="{{ $category->id }}">
		   		<div class="form-group">
		   			<label>Category Name</label>
		   			<input type="text" name="name" class="form-control" value="{{ $category->name }}" required>
		   		</div>
		   		<button class="btn btn-warning">Update</button>
		   </form>
	  	</div>
	</div>
	@endsection
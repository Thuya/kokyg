@extends('layouts/adminview')
	@section('content')
	<div class="card position-relative mt-5">
	  	<div class="card-header">
	  		<div class="p-3 position-absolute rounded" style="top: -24px; margin-bottom: -24px; background: #000;">
	  			<div>
	  				<i aria-hidden="true" class="fas fa-warehouse" style="font-size: 40px; color: #fff;"></i>
	  			</div>
	  		</div>
	  		<h3 style="margin-left: 89px;">Purchase</h3>
	  	</div>
	  	<div class="card-body">
		   <form action="{{ route('purchases.create', $purchase->id) }}" method="POST">
		   		{{ csrf_field() }}
		   		<input type="hidden" name="id" value="{{ $purchase->id }}">
		   		<div class="form-group">
		   			<label>Product Name</label>
		   			<input type="text" name="name" value="{{ $purchase->name }}" class="form-control" readonly>
		   				@if ($errors->has('name'))
			   				<div class="alert alert-danger mt-2" role="alert">
			   					{{ $errors->first('name') }}
			   				</div>
		   				@endif
		   		</div>
		   		<div class="form-group">
		   			<label>Company</label>
		   			<select class="form-control">
		   				<option>TMW Distribution</option>
		   				<option>TNL Distribution</option>
		   				<option>KKL Distribution</option>
		   			</select>
		   		</div>
		   		<div class="form-group">
		   			<label>Remain Quantity</label>
		   			<input type="number" value="{{ $purchase->totalqty }}" class="form-control" readonly>
		   		</div>
		   		<div class="form-group">
		   			<label>Want to add Quantity</label>
		   			<input type="number" name="totalqty" class="form-control">
		   		</div>
		   		<button class="btn btn-success">Purchase</button>
		   </form>
	  	</div>
	</div>
	@endsection
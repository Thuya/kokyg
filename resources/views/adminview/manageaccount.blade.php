@extends('layouts/adminview')
	@section('content')
	@if(session()->has('danger'))
		<div class="alert alert-danger">
			{{ session('danger') }}
		</div>
	@endif
	@if(session()->has('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
	@endif
	<div class="card position-relative mt-5">
	  	<div class="card-header">
	  		<div class="p-3 position-absolute rounded" style="top: -24px; margin-bottom: -24px; background: #000;">
	  			<div class="pa-4 v-card theme--dark green elevation-10">
	  				<i aria-hidden="true" class="fas fa-user-cog" style="font-size: 40px; color: #fff;"></i>
	  			</div>
	  		</div>
	  		<h3 style="margin-left: 89px;">Manage Account</h3>
	  	</div>
	  	<div class="card-body">
		   <form action="{{ route('admin.accupdate', Auth::user()->id) }}" method="POST">
		   		{{ csrf_field() }}
		   		<input type="hidden" name="id" value="{{ Auth::user()->id }}">
		   		<div class="form-group">
		   			<label>Name</label>
		   			<input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" >
		   		</div>
		   		<div class="form-group">
		   			<label>Email</label>
		   			<input type="email" class="form-control" value="{{ Auth::user()->email }}" readonly>
		   		</div>
		   		<div class="form-group">
		   			<label>Old Password</label>
		   			<input type="password" name="old_pw" class="form-control">
		   		</div>
		   		<div class="form-group">
		   			<label>New Password</label>
		   			<input type="password" name="new_pw" class="form-control">
		   		</div>
		   		<button class="btn btn-success">Update</button>
		   </form>
	  	</div>
	</div>
	@endsection
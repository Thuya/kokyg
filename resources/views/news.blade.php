@extends('layouts/storeview')
	@section('content')
	<div class="container">
		<div class="shadow-box shadow mb-5 bg-white rounded p-4">
			<div class="border-bottom pb-4 mb-4">
				<img class="card-img-top" style="height: 300px;" src="https://c8.alamy.com/comp/HDJJDX/close-up-of-woman-hands-using-mobile-phone-and-laptop-computer-with-HDJJDX.jpg" alt="Card image cap">
				<h1 class="text-center m-3">Hello World</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi similique suscipit praesentium placeat, qui incidunt est unde natus ex, quaerat quasi eius porro at voluptatum sint dicta, fuga possimus beatae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam tempore nobis rem, quam autem molestiae necessitatibus? Iusto molestias libero error tempora pariatur facere. Exercitationem reprehenderit facere odit dolorum laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus aperiam nulla eos, eum rerum repudiandae ducimus...
					<div class="collapse" id="collapseExample">
						 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.accusantium aliquam sed placeat a dolores! Voluptatibus in consequatur unde quisquam, est excepturi, veniam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum minus eligendi similique consequatur commodi error repellat, saepe nisi quibusdam iure debitis, explicabo amet temporibus quia id eos, atque rem blanditiis!
					</div>
				</p>
				<div class="text-center">
					<button class="btn btn-inverse btn-dark btn-lg font-small cart-button" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
					    Continue Reading
					</button>
				</div>
			</div>
			<div class="border-bottom pb-4 mb-4">
				<img class="card-img-top" style="height: 300px;" src="https://c8.alamy.com/comp/HDJJDX/close-up-of-woman-hands-using-mobile-phone-and-laptop-computer-with-HDJJDX.jpg" alt="Card image cap">
				<h1 class="text-center m-3">Hello World</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi similique suscipit praesentium placeat, qui incidunt est unde natus ex, quaerat quasi eius porro at voluptatum sint dicta, fuga possimus beatae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam tempore nobis rem, quam autem molestiae necessitatibus? Iusto molestias libero error tempora pariatur facere. Exercitationem reprehenderit facere odit dolorum laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus aperiam nulla eos, eum rerum repudiandae ducimus...
					<div class="collapse" id="collapse2">
						 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.accusantium aliquam sed placeat a dolores! Voluptatibus in consequatur unde quisquam, est excepturi, veniam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum minus eligendi similique consequatur commodi error repellat, saepe nisi quibusdam iure debitis, explicabo amet temporibus quia id eos, atque rem blanditiis!
					</div>
				</p>
				<div class="text-center">
					<button class="btn btn-inverse btn-dark btn-lg font-small cart-button" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
					    Continue Reading
					</button>
				</div>
			</div>
			<div class="border-bottom pb-4 mb-4">
				<img class="card-img-top" style="height: 300px;" src="https://c8.alamy.com/comp/HDJJDX/close-up-of-woman-hands-using-mobile-phone-and-laptop-computer-with-HDJJDX.jpg" alt="Card image cap">
				<h1 class="text-center m-3">Hello World</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi similique suscipit praesentium placeat, qui incidunt est unde natus ex, quaerat quasi eius porro at voluptatum sint dicta, fuga possimus beatae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam tempore nobis rem, quam autem molestiae necessitatibus? Iusto molestias libero error tempora pariatur facere. Exercitationem reprehenderit facere odit dolorum laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus aperiam nulla eos, eum rerum repudiandae ducimus...
					<div class="collapse" id="collapse3">
						 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.accusantium aliquam sed placeat a dolores! Voluptatibus in consequatur unde quisquam, est excepturi, veniam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum minus eligendi similique consequatur commodi error repellat, saepe nisi quibusdam iure debitis, explicabo amet temporibus quia id eos, atque rem blanditiis!
					</div>
				</p>
				<div class="text-center">
					<button class="btn btn-inverse btn-dark btn-lg font-small cart-button" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
					    Continue Reading
					</button>
				</div>
			</div>
			<div class="border-bottom pb-4 mb-4">
				<img class="card-img-top" style="height: 300px;" src="https://c8.alamy.com/comp/HDJJDX/close-up-of-woman-hands-using-mobile-phone-and-laptop-computer-with-HDJJDX.jpg" alt="Card image cap">
				<h1 class="text-center m-3">Hello World</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi similique suscipit praesentium placeat, qui incidunt est unde natus ex, quaerat quasi eius porro at voluptatum sint dicta, fuga possimus beatae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam tempore nobis rem, quam autem molestiae necessitatibus? Iusto molestias libero error tempora pariatur facere. Exercitationem reprehenderit facere odit dolorum laudantium! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus aperiam nulla eos, eum rerum repudiandae ducimus...
					<div class="collapse" id="collapse4">
						 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.accusantium aliquam sed placeat a dolores! Voluptatibus in consequatur unde quisquam, est excepturi, veniam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum minus eligendi similique consequatur commodi error repellat, saepe nisi quibusdam iure debitis, explicabo amet temporibus quia id eos, atque rem blanditiis!
					</div>
				</p>
				<div class="text-center">
					<button class="btn btn-inverse btn-dark btn-lg font-small cart-button" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
					    Continue Reading
					</button>
				</div>
			</div>
		</div>
	</div>
	@endsection
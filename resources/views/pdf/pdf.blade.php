<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Technoland</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="logo">
			<a href="{{ url('/') }}" style="color: red; font-style: italic;"><h1>Technoland</h1></a>
		</div>
		<div class="text-justify">
			<table class="table">
				<tr>
					<td>Product :</td>
					<td><strong>
						@foreach($order->orderdetails as $detail)
							{{ $detail->product->name }}
						@endforeach
						</strong></td>
				</tr>
				<tr>
					<td>Order Date : </td>
					<td><strong>{{ $order->order_date }}</strong></td>
				</tr>
				<tr>
					<td>Total Quantity : </td>
					<td><strong>{{ $order->total_quantity }}</strong></td>
				</tr>
				<tr>
					<td>Total Price :</td>
					<td><strong>{{ $order->total_amount }}</strong></td>
				</tr>
			</table>
		</div>
		<p><strong>Thank you for your booking.</strong></p>
	</div>

</body>
</html>
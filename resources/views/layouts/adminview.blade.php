<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Shirt</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<style type="text/css">
		body {
			font-family: Montserrat,Helvetica Neue,Arial,sans-serif;
			background: #ebecf1;
		}
		.nav-bar.active1 {
	        min-width: 80px;
	        max-width: 80px;
	        text-align: center;
	        margin-left: -8px !important;
	    }
	    .nav-bar.active1 .nav-lists b {
	        display: none;
	    }
	    .nav-bar.active1 .nav-lists i {
	    	text-align: center;
	    	margin-left: 10px;
	    	font-size: 20px;
	    }
	    .nav-bar.active1 .heading-nav strong {
	    	display: none;
	    }
	    .nav-bar.active1 .heading-nav h1 {
	    	margin-left: -5px;
    		font-size: 25px;
	    }
	    #sidebar.active .sidebar-heading h2 {
	        display: block;
	    }
	    .wrapper.active1 {
	    	margin-left: -10px !important;
	    }
	    .margin-left.active1 {
			margin-left: 0;
		}
		.margin-left {
			margin-left: 260px;
		}
	    /* Reappearing the sidebar on toggle button click */
	    .active1 {
	        margin-left: 71px !important; 
	    }
	    .color-box {
	    	display: none;
	    }
	</style>
</head>
<body>
	<!-- menu-bar -->
	<div class="menu">
		<div class="row">
			<div class="col-md-6">
				<button id="sidebarCollapse"><i class="fas fa-ellipsis-v"></i></button> ADMIN DASHBOARD
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-8">
						<div class="search-admin">
							<form action="{{ route('admin.search') }}" class="d-flex" method="POST">
								{{ csrf_field() }}
								<input type="search" name="search" class="admin-search" placeholder="Search...">
								<i class="fas fa-search" id="sidebarCollapse"></i>
							</form>
						</div>
					</div>
					<div class="col-md-4">
						<ul class="list-inline icons">
							<li><a href="{{ url('/') }}"><i class="fas fa-weight-hanging"></i></a></li>
							<li><a href="{{ route('admin.account') }}"><i class="fas fa-user-cog"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- menu-bar -->

	<!-- side-bar -->
	<div class="nav-bar">
		<div class="heading-nav" id="po-rel">
			<h1>Cat <strong>Admin</strong></h1>
		</div>
		<ul class="list-unstyled" id="po-rel">
			<li class="nav-lists"><a href="{{ route('products') }}"><i class="fas fa-lock"></i><b>Manage Products</b></a></li>
			<li class="nav-lists"><a href="{{ route('colors') }}"><i class="fas fa-palette"></i><b>Manage Colors</b></a></li>
			<li class="nav-lists"><a href="{{ route('categories') }}"><i class="fas fa-calendar"></i><b>Manage Category</b></a></li>
			<li class="nav-lists"><a href="{{ route('brands') }}"><i class="fab fa-apple""></i><b>Manage Brand</b></a></li>
			<li class="nav-lists"><a href="{{ route('admin.delivery') }}"><i class="fas fa-truck""></i><b>Manage Delivery</b></a></li>
			<li class="nav-lists"><a href="{{ route('orders') }}"><i class="fas fa-truck""></i><b>Manage Order</b></a></li>
			<li class="nav-lists"><a href="{{ route('reports') }}"><i class="fas fa-envelope"></i><b>View Report</b></a></li>
			<li class="nav-lists">
				<a href="" onclick="event.preventDefault();
					           document.getElementById('logout-form').submit();">
					<i class="fas fa-sign-out-alt"></i><b>LogOut</b>
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				    @csrf
				</form>
			</li>
		</ul>
	</div>

	<!-- body -->
	<div class="margin-left">
		<div class="container">
			<div class="wrapper">
				@yield('content')
			</div>
		</div>
	</div>
	<!-- body -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
	$(document).ready(function () {
		$('#sidebarCollapse').on('click', function () {
			$('.nav-bar,.menu,.wrapper,.margin-left').toggleClass('active1');
		});

		$('#filter').change(function() {
			$("#filter-form").submit();
		});

	   	$('.js-example-basic-single').select2();
	});
</script>
</body>
</html>
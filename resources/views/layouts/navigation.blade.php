<div>
	<div class="top-navi mb-4 text-muted">
		<div class="container d-flex pt-3 pb-1">
			<div class="flex-fill">
				<div class="dropdown">
				  	<a class="text-decoration-none text-muted dropdown-toggle" href="#" role="button" id="account" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				   		<i class="fas fa-user pr-1"></i>
							My Account
					</a>
				  	<div class="dropdown-menu" aria-labelledby="account">
				  		@guest
					    <a  class="dropdown-item" href="{{ route('login') }}">Login</a>
					    <a href="{{ route('register') }}" class="dropdown-item">Register</a>
					    @else
						    @if(Auth::user()->role_id == 1)
						    <a href="{{ route('products') }}" class="dropdown-item">{{ Auth::user()->name }}</a>
						    @else
						    <a href="#" class="dropdown-item">{{ Auth::user()->name }}</a>
						    @endif
					    <a class="dropdown-item" href="{{ route('logout') }}"
					       onclick="event.preventDefault();
					                     document.getElementById('logout-form').submit();">
					        {{ __('Logout') }}
					    </a>
					    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					        @csrf
					    </form>
					    @endguest
				  	</div>
				</div>
			</div>
			<div class="flex-fill text-right top-navi-right">
				<ul class="list-inline">
					<li class="list-inline-item mr-4">
						<a href="#" class="text-muted text-decoration-none">
							<i class="fas fa-phone pr-1"></i>
							123456789
						</a>
					</li>
					<li class="list-inline-item mr-4">
						<a href="{{ route('wish') }}" class="text-muted text-decoration-none">
							<i class="fas fa-heart pr-1"></i>
							Wish List ( {{ count(session('wish')) }} )
						</a>
					</li>
					<li class="list-inline-item mr-4">
						<a href="{{ route('carts') }}" class="text-muted text-decoration-none">
							<i class="fas fa-shopping-cart pr-1"></i>
							ShoppingCart
						</a>
					</li>
					<li class="list-inline-item mr-4">
						<a href="#" class="text-muted text-decoration-none">
							<i class="fas fa-share pr-1"></i>
							Checkout
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="logo-and-search mb-2">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h1 class="logo">​
						<a href="/" class="text-decoration-none"><i>TECHNOLAND</i></a>
					</h1>
				</div>
				<div class="col-md-5">
					<form action="{{ route('products.search') }}" method="GET">
						{{ csrf_field() }}
						<div class="input-group input-group-lg mb-2">
							<input type="text" name="search" class="form-control font-small" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" placeholder="Search...">
						  	<div class="input-group-prepend">
						    	<span class="input-group-text font-small" id="inputGroup-sizing-lg">
						    		<i class="fas fa-search"></i>
						    	</span>
						  	</div>
						</div>
					</form>
				</div>
				<?php $total = 0.00 ?>
				@if(session('cart'))
				    @foreach(session('cart') as $id => $details)
				        <?php $total += $details['price'] * $details['quantity'] ?>
				    @endforeach
				@endif
				<div class="col-md-3">
					<div class="dropdown">
						<button id="cart" type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn-block btn btn-inverse btn-dark btn-block btn-lg font-small cart-button">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							<span id="cart-total">{{ count(session('cart')) }} item(s) - $ {{ $total }}</span>
						</button>
					    <div class="dropdown-menu w-100">
					    	<div class="container">
						        <div class="row total-header-section p-2 border-bottom mb-3">
						            <div class="col-lg-4 col-sm-4 col-4">
						                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count(session('cart')) }}</span>
						            </div>

						            <div class="col-lg-8 col-sm-8 col-8 total-section text-right">
						                <p>Total: <span class="text-info">$ {{ $total }}</span></p>
						            </div>
						        </div>
						        @if(session('cart'))
						            @foreach(session('cart') as $id => $details)
						                <div class="row cart-detail mb-3">
						                    <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
						                        <img src="{{ $details['photo'] }}" style="height: 90px;" class="w-100" />
						                    </div>
						                    <div class="col-lg-8 col-sm-8 col-8 cart-detail-product pt-3">
						                        <p class="m-0">{{ $details['name'] }}</p>
						                        <span class="count"> Quantity:{{ $details['quantity'] }}</span> <span class="price text-info"> ${{ $details['price'] }}</span>
						                    </div>
						                </div>
						            @endforeach
						        @endif
						        <div class="row">
						            <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
						                <a href="{{ route('carts') }}" class="btn btn-inverse btn-dark btn-block btn-lg font-small cart-button">View all</a>
						            </div>
						        </div>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-navi">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  			<a class="navbar-brand d-block d-sm-none d-md-none" href="#">Categories</a>
	  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menutabs" aria-controls="menutabs" aria-expanded="false" aria-label="Toggle navigation">
	  		  		<span class="navbar-toggler-icon"></span>
	  			</button>

			  	<div class="collapse navbar-collapse font-small" id="menutabs">
			    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			      		<li class="nav-item pr-3">
			        		<a class="nav-link" href="/">Home</a>
			      		</li>
				      	<li class="nav-item pr-3">
				      		<div class="dropdown">
				      			<a class="nav-link" id="laptops" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All Products <span class="sr-only">(current)</span></a>
				      			<div class="dropdown-menu font-small" aria-labelledby="laptops">
				      				@forelse( $categories as $category )
				      				<a class="dropdown-item" href="{{ route('products.' . $category->name, $category->id) }}">{{ $category->name }}</a>
				      				@empty
									<a class="dropdown-item" href="{{ route('products.phone') }}">Phone</a>
				      				@endforelse
				      				<a class="dropdown-item" href="{{ route('products.all') }}">Show all Laptops & Notebooks</a>
			        		  	</div>
				      		</div>
				      	</li>
			      		<li class="nav-item pr-3">
			        		<a class="nav-link" href="{{ url('/about') }}">About</a>
			      		</li>
			      		<li class="nav-item pr-3">
			        		<a class="nav-link" href="{{ url('/service') }}">Our Services</a>
			      		</li>
			      		<li class="nav-item pr-3">
			        		<a class="nav-link" href="{{ url('/news&events') }}">News & Events</a>
			      		</li>
			      		<li class="nav-item pr-3">
			        		<a class="nav-link" href="{{ url('/contact') }}">Contact Us</a>
			      		</li>
			      		<li class="nav-item pr-3">
			        		<a class="nav-link" href="{{ route('history') }}">My History</a>
			      		</li>
			    	</ul>
			  	</div>
			</nav>
		</div>
	</div>
</div>
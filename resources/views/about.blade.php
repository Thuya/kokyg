@extends('layouts/storeview')
	@section('content')
	<div class="container">
		<div class="shadow-box shadow mb-5 bg-white rounded p-4">
			<h5>Company Profile</h5>
			<p>
				U Myat Soe Win is an entrepreneur, one of Myanmar’s best-known electronic and IT retailers, a family man, Founder and Managing Director of Technoland Computer Trading Company Limited.
			</p>
			<p>
				U Myat Soe Win started Technoland with one store in Yangon, Myanmar in March 2000. During the next 17 years U Myat Soe Win expanded the business to a total of 12 stores including showrooms and service centers across Myanmar.
			</p>
			<p>
				Given the recent fast-moving reforms in Myanmar we believe that this will be the springboard for massive growth of the Technoland brand which is the most trusted and recognizable electronic retail and IT dealer in the Myanmar market.
			</p>
			<h5>Kyg Kyi – The Company</h5>
			<ul>
				<li>
					Offers the widest range of products and services from Smartphones, Tablets, Laptops, Desktops, Projectors, Printers, Scanners, Network appliances, Computer peripherals, other related Gadgets and Accessories. Technoland also provides survey equipment, photo frames and a one-stop photo service.
				</li>
				<li>
					Access to well-known brands such as Hewlett-Packard (HP), Dell, Apple, Cisco, Ruckus, UBiQUiTi, HIK Vision, Jabra, B&O Play, JVC, Sony, Panasonic, Canon, Epson, Fuji Xerox, Brother, Acer, Samsung, LG, Lenovo, HTC, AOC, ASUS, Kaspersky, ViewSonic, and more.
				</li>
				<li>
					There are 12 Technoland Stores for customers in 5 cities.
				</li>
				<li>
					According to the ICT Survey 2009 and 2010, Technoland was awarded “Best Service Award” by Myanmar Computer Industrial Association (MCIA) in 2009 and 2010. Technoland became Gold Intel Technology Provider (ITP) in 2014 and Platinum ITP in 2016.
				</li>
				<li>
					Successful business to business distributor relationships with well-known local companies such as KMD, DCD, TMW, ShweLamin Naga, Synnex Myanmar, Royal Irrawaddy, Alliance Star, Royal Smart, Concordia, Client Focus, Myanmar Golden Rock, Gandamar Office Machine Ltd, Mastech, MMG Network Solution, Frontiir companies and other retail outlets.
				</li>
				<li>
					Technoland employs more than 310 Sales, Technicians and Office Staff.
				</li>
				<li>
					Technoland signed an agreement with a large Japanese multi-national to provide finance under a small loan scheme and partnership with Myanmar’s largest supermarket chain for establishment of outlets within their supermarkets.
				</li>
			</ul>

			<h5>Company Structure</h5>
			Technoland has six company departments: <br>
			1.	Administration Department<br>
			2.	Human Resource Department<br>
			3.	Finance Department<br>
			4.	IT Sales and Marketing Department split into:<br>
			a.	Mobile section<br>
			b.	Computer section<br>
			c.	Printer and Network section<br>
			d.	Accessories section<br>
			e. B2B/B2C section<br>
			5.	IT Service Center<br>
			a.	Networking & Outdoor Service section<br>
			b.	One stop in-store servicing section (Computer, Mobile & Printer)<br>
			6. Business Development Department
		</div>
	</div>
	@endsection
@extends('layouts/storeview')
	@section('content')
	<div class="container">
		<div class="shadow-box shadow mb-5 bg-white rounded p-4">
			<h5>Service</h5>
			<p>
				Technoland Computer Trading Co.,Ltd. actually takes pride of maintaining a personalized sales and technical services. The company is very committed to a very high level of customer satisfaction and services, even through after sales.
			</p>
			<p>		
				To cater steadfast and consistent support of services, the company has maintained teams of skilled-trained technicians and expert engineers in order to have unwavering, prompt and timely technical support even through after sales. Our fleet of delivery vehicles also renders a key factor.
			</p>
			<p>
				Our services are as follow.
			</p>
			<h5>PC and Notebook Services</h5>
			<ul>
				<li>
					Windows /Mac OS installation
				</li>
				<li>
					Mac + Windows Parallel OS installation
				</li>
				<li>
					Hardware /Software installation
				</li>
				<li>
					Hardware /Software troubleshooting
				</li>
				<li>
					Hardware Checking/Upgrade
				</li>
				<li>
					Virus Checking
				</li>
				<li>
					Data Recovery
				</li>
				<li>
					Network installation/Configuration
				</li>
				<li>
					Router Configuration
				</li>
			</ul>

			<h5>Mobile/Tablet</h5>
			<li>Firmware installation/upgrade</li>
			<li>OS / Software installation</li>
			<li>Software troubleshooting</li>
			<li>Create Apple ID</li>
			<li>Hardware Checking for iPhone/iPad</li>
		</div>
	</div>
	@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/service', function () {
    return view('service');
});
Route::get('/contact', function () {
    return view('contactus');
});
Route::get('/news&events', function () {
    return view('news');
});

Route::group([ 'middleware' => ['auth', 'role']], function ()
{
	Route::get('/admin/colors', 'ColorController@index')->name('colors');
	Route::get('/admin/colors/create', 'ColorController@create')->name('colors.create');
	Route::post('/admin/colors/store', 'ColorController@store')->name('colors.store');
	Route::get('/admin/colors/edit/id={id}', 'ColorController@edit')->name('colors.edit');
	Route::post('/admin/colors/update/id={id}', 'ColorController@update')->name('colors.update');
	Route::get('/admin/colors/delete/id={id}', 'ColorController@delete')->name('colors.delete');

	Route::get('/admin/suppliers/create', 'SupplierController@create')->name('suppliers.create');
	Route::post('/admin/suppliers/store', 'SupplierController@store')->name('suppliers.store');
	Route::get('/admin/suppliers/edit/id={id}', 'SupplierController@edit')->name('suppliers.edit');
	Route::post('/admin/suppliers/update/id={id}', 'SupplierController@update')->name('suppliers.update');
	Route::get('/admin/suppliers/delete/id={id}', 'SupplierController@delete')->name('suppliers.delete');

	Route::get('/admin/brands', 'BrandController@index')->name('brands');
	Route::get('/admin/brands/create', 'BrandController@create')->name('brands.create');
	Route::post('/admin/brands/store', 'BrandController@store')->name('brands.store');
	Route::get('/admin/brands/edit/id={id}', 'BrandController@edit')->name('brands.edit');
	Route::post('/admin/brands/update/id={id}', 'BrandController@update')->name('brands.update');
	Route::get('/admin/brands/delete/id={id}', 'BrandController@delete')->name('brands.delete');

	Route::get('/admin/categories', 'CategoryController@index')->name('categories');
	Route::get('/admin/categories/create', 'CategoryController@create')->name('categories.create');
	Route::post('/admin/categories/store', 'CategoryController@store')->name('categories.store');
	Route::get('/admin/categories/edit/id={id}', 'CategoryController@edit')->name('categories.edit');
	Route::post('/admin/categories/update/id={id}', 'CategoryController@update')->name('categories.update');
	Route::get('/admin/categories/delete/id={id}', 'CategoryController@delete')->name('categories.delete');

	Route::get('/admin/products', 'ProductController@index')->name('products');
	Route::get('/admin/products/create', 'ProductController@create')->name('products.create');
	Route::post('/admin/products/store', 'ProductController@store')->name('products.store');
	Route::get('/admin/products/show/id={id}', 'ProductController@show')->name('products.show');
	Route::get('/admin/products/edit/id={id}', 'ProductController@edit')->name('products.edit');
	Route::post('/admin/products/update/id={id}', 'ProductController@update')->name('products.update');
	Route::get('/admin/products/delete/id={id}', 'ProductController@delete')->name('products.delete');
	Route::post('/admin/products/search', 'ProductController@adminsearch')->name('admin.search');

	Route::get('/admin/orders', 'HistoryController@index')->name('orders');
	Route::get('/admin/reports', 'ReportController@index')->name('reports');
	Route::get('/admin/purchases/product={id}', 'PurchaseController@create')->name('purchases');
	Route::post('/admin/purchases/update/product={id}', 'PurchaseController@store')->name('purchases.create');
	Route::get('/admin/reports/generate/all', 'ReportController@reportall')->name('reports.all');
	Route::get('/admin/reports/daily', 'ReportController@daily')->name('reports.daily');
	Route::get('/admin/reports/monthly', 'ReportController@monthly')->name('reports.monthly');
	Route::get('/admin/manage/account', 'HomeController@account')->name('admin.account');
	Route::post('/admin/manage/account/update/id={id}', 'HomeController@accupdate')->name('admin.accupdate');
	Route::get('/admin/deliveries', 'DeliveryController@index')->name('admin.delivery');
	Route::get('/admin/deliveries/update/status={id}', 'DeliveryController@status')->name('delivery.status');
});

Route::group([ 'middleware' => ['auth']], function ()
{
	Route::get('/products/phone/{id}', 'ProductController@phone')->name('products.phone');
	Route::get('/products/laptop/{id}', 'ProductController@phone')->name('products.laptop');
	Route::get('/products/tablet/{id}', 'ProductController@phone')->name('products.tablet');
	Route::get('/products/ipad/{id}', 'ProductController@phone')->name('products.ipad');
	Route::get('/products/MP3/{id}', 'ProductController@phone')->name('products.MP3');
	Route::get('/products/TV/{id}', 'ProductController@phone')->name('products.TV');
	Route::get('/products/Mac/{id}', 'ProductController@phone')->name('products.Mac');
	Route::get('/products/Printer/{id}', 'ProductController@phone')->name('products.Printer');
	Route::get('/products/Projector/{id}', 'ProductController@phone')->name('products.Projector');
	Route::get('/products/all', 'ProductController@allproducts')->name('products.all');

	Route::get('/carts', 'CartController@allcarts')->name('carts');
	Route::get('/products/id={id}', 'ProductController@detail')->name('products.detail');

	Route::post('/add-to-cart/', 'CartController@addtocart');
	Route::patch('update-cart', 'CartController@update');
	Route::delete('remove-from-cart', 'CartController@remove');

	Route::post('/wish/add', 'CartController@wish');
	Route::post('/wish/remove', 'CartController@wishremove');
	Route::get('/wish', 'CartController@allwish')->name('wish');

	Route::post('/checkout/products', 'OrderController@checkout')->name('checkout');

	Route::get('/buy/products/order={id}', 'OrderController@buyproduct')->name('buy.product');

	Route::get('my-history', 'HistoryController@history')->name('history');
	Route::get('/products/search', 'CartController@search')->name('products.search');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:products', 
            'price' => 'required',
            'totalqty' => 'required',
            'description' => 'required',
            'photo' => 'required',
            'cpu' => 'required',
            'ram' => 'required',
            'screen_display' => 'required',
            'resolution' => 'required',
            'storage' => 'required',
            'operating_system' => 'required',
            'camera' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Need to fill this field!', 
            'price.required' => 'Need to fill this field!',
            'totalqty.required' => 'Need to fill this field!',
            'description.required' => 'Need to fill this field!',
            'photo.required' => 'Need to fill this field!',
            'cpu.required' => 'Need to fill this field!',
            'ram.required' => 'Need to fill this field!',
            'screen_display.required' => 'Need to fill this field!',
            'resolution.required' => 'Need to fill this field!',
            'storage.required' => 'Need to fill this field!',
            'operating_system.required' => 'Need to fill this field!',
            'camera.required' => 'Need to fill this field!',
        ];  
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\Product;
use Validator;
use Mail;
use PDF;
use Auth;
use App\Delivery;

class OrderController extends Controller
{
    public function checkout(Request $request)
    {
      // dd($request);
    	$messages = [
    		'phone.required' => 'Need to fill this field!',
    		'address.required' => 'Need to fill this field!',
    	];
    	$validator = Validator::make($request->all(), [
            'phone' => 'required',
            'address' => 'required',
        ], $messages)->validate();

        $carts = session()->get('cart');
        $order = Order::create($request->all());
        $delivery = Delivery::create([
            'fees' => $request->total_amount,
            'date' => $request->order_date,
            'contact_person' => Auth::user()->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'order_id' => $order->id
        ]);

        foreach ($carts as $cart => $value) {
        	$orderdetail = OrderDetail::create([
		    	'order_id' => $order->id,
  				'product_id' => $cart,
  				'item_name' => $value['name'],
  				'quantity' => $value['quantity'],
  				'price' => $value['price'],
        	]);
            $product = Product::find($cart);
            $minusqty = $product->totalqty - $orderdetail->quantity;
            $product->update([
                'totalqty' => $minusqty,
            ]);
        }

        session()->forget('cart');
        $pdf = PDF::loadView('pdf.pdf', [ 'order' => $order ]);
        $pdf->setPaper('A4', 'landscape');
        Mail::raw('Thank You for buying products.', function($message) use($pdf)
        {
            $message->subject('KygKyi IT Devices');
            $message->from('no-reply@kygkyiIT.com', 'KygKyi');
            $message->to('kyaunglanhtan98@gmail.com');
            $message->attachData($pdf->output(), "confirmation.pdf");
        }); 
        return redirect('/buy/products/order=' . $order->id);
    }

    public function buyproduct($id)
    {
        $buy = Order::find($id);
        $buys = Order::find($id)->orderdetails;
        return view('userview.buyproduct', ['buy' => $buy, 'buys' => $buys]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use PDF;
use App\Order;
use Carbon\Carbon;
use App\Supplier;

class ReportController extends Controller
{
    public function index()
    {
    	$reports = Product::all();
        $suppliers = Supplier::all();
    	return view('adminview.reports.index', [
    		'reports' => $reports,
            'suppliers' => $suppliers
    	]);
    }

    public function reportall()
    {
    	$reports = Order::all();
    	$pdf = PDF::loadView('adminview.reports.all', [ 'reports' => $reports ]);
        $pdf->setPaper('A4', 'landscape');
       	return $pdf->download('reports.pdf');
    }

    public function daily()
    {
    	$today = Carbon::now()->toDateString();
    	$reports = Order::where('order_date', $today)->get();
    	$pdf = PDF::loadView('adminview.reports.daily', [ 'reports' => $reports ]);
        $pdf->setPaper('A4', 'landscape');
       	return $pdf->download('reports-daily.pdf');
    }
    public function monthly()
    {
        $month = Carbon::now()->month;
        $reports = Order::where('order_month', $month)->get();
        $pdf = PDF::loadView('adminview.reports.monthly', [ 'reports' => $reports ]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('reports-monthly.pdf');
    }
}
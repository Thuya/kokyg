<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use Validator;

class SupplierController extends Controller
{
    public function create()
    {
    	return view('adminview.suppliers.create');
    }

    public function store(Request $request)
    {
    	$messages = [
    		'name.required' => 'Need to fill this field!',
    	];
    	$validator = Validator::make($request->all(), [
            'name' => 'required|unique:suppliers',
        ], $messages)->validate();
    	Supplier::create([
    		'name' => $request->name
    	]);
    	return redirect('/admin/reports');
    }

    public function edit($id)
    {
    	$supplier = Supplier::find($id);
    	return view('adminview.suppliers.edit', [ 'supplier' => $supplier ]);
    }

    public function update(Request $request, $id)
    {
    	$supplier = Supplier::find($id);
    	$supplier->update([
    		'name' => $request->name
    	]);
    	return redirect('/admin/reports');
    }

    public function delete($id)
    {
    	$supplier = Supplier::find($id);
    	$supplier->delete();
    	return redirect('/admin/reports');
    }
}

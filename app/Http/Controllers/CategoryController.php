<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    public function index()
	{
		$categories = Category::all();
		return view('adminview/categories/index', ['categories' => $categories]);
	}

    public function create()
    {
    	return view('adminview/categories/create');
    }

    public function store(Request $request)
    {
    	$messages = [
    		'name.required' => 'Need to fill this field!',
    	];
    	$validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories',
        ], $messages)->validate();
    	Category::create($request->all());
    	return redirect('/admin/categories')
    	->with('message', 'Your categories is successfully created!')
    	->with('status', 'success');
    }

    public function edit($id)
    {
    	$category = Category::find($id);
    	return view('/adminview/categories/edit', [ 'category' => $category ]);
    }

    public function update(Request $request, $id)
    {
    	$category = Category::find($id);
    	$category->update($request->all());
    	return redirect('/admin/categories')
    	->with('message', 'Your category is successfully updated!')
    	->with('status', 'warning');
    }

    public function delete($id)
    {
    	$category = Category::find($id);
    	$category->delete();

    	return redirect('/admin/categories')
    	->with('message', 'Your category is successfully deleted!')
    	->with('status', 'danger');
    }
}

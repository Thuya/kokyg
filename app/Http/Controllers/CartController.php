<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
class CartController extends Controller
{
    public function addtocart(Request $request)
    {
        // var_dump($request);
    	$product = Product::find($request->id);

    	if (!$product) {
    		abort(404);
    	}
    	$cart = session()->get('cart');
    	if (! $cart) {
    		$cart = [
    			$request->id => [
    				'name' => $product->name,
    				'totalqty' => $product->totalqty,
    				'quantity' => 1,
    				'price' => $product->price,
    				'photo' => $product->photo
    			]
    		];

    		session()->put('cart', $cart);
    		session()->flash('success', 'Product added to cart successfully!');
    	}
        // var_dump(isset($cart[$request->id]));
    	elseif (isset($cart[$request->id])) {
    		$cart[$request->id]['quantity']++;
    		session()->put('cart', $cart);
    		session()->flash('success', 'Product added to cart successfully!');
    	} else {
                $cart[$request->id] = [
                    'name' => $product->name,
                    'totalqty' => $product->totalqty,
                    'quantity' => 1,
                    'price' => $product->price,
                    'photo' => $product->photo
                ];
                session()->put('cart', $cart);
                session()->flash('success', 'Product added to cart successfully!');
        }
    }

    public function update(Request $request)
    {
        if ( $request->id && $request->quantity ) {

            $cart = session()->get('cart');

            $cart[$request->id]['quantity'] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');

            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function wish(Request $request)
    {
    	$product = Product::find($request->id);
    	if (! $product) {
    		abort(404);
    	}
    	$wish = session()->get('wish');
    	if (! $wish) {
    		$wish = [
    			$request->id => [
    				'id' => $product->id,
    				'name' => $product->name,
    				'totalqty' => $product->totalqty,
    				'price' => $product->price,
    				'photo' => $product->photo
    			]
    		];
    		session()->put('wish', $wish);
    		return redirect()->back()->with('success', 'Product added to wish list!');
    	}
    	$wish[$request->id] = [
    		'name' => $product->name,
    		'totalqty' => $product->totalqty,
    		'price' => $product->price,
    		'photo' => $product->photo
    	];
    	$wishlist = session()->put('wish', $wish);
    	return redirect()->back()->with('success', 'Product added to wish list!');
    }
    public function allcarts()
    {
        $month = Carbon::now()->month;
        return view('/userview/cart', [
            'month' => $month
        ]);
    }

    public function allwish()
    {
        return view('/userview/wish');
    }

    public function wishremove(Request $request)
    {
    	if ($request->id) {
    		$wish = session()->get('wish');
    		if(isset($wish[$request->id])) {

                unset($wish[$request->id]);

                session()->put('wish', $wish);
            }
            return redirect()->back()->with('success', 'Product removed from wish list!');
    	}
    }

    public function search(Request $request)
    {
        // dd($request);
        $product_search = $request->search;
        $phones = Product::where('name', 'LIKE', '%' . $product_search . '%')->get();

        return view('/userview/phone', [ 'phones' => $phones ]);
    }
}

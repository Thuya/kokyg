<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use Auth;
class HistoryController extends Controller
{
	public function index()
	{
		$orders = Order::all();
		return view('adminview.orders.index', [ 'orders' => $orders ]);
	}
    public function history()
    {
    	$orders = Order::with('user', 'orderdetails')->where('user_id', Auth::user()->id)->get();
    	return view('userview.history', [ 'orders' => $orders ]);
    }
}

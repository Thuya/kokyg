<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use Validator;

class BrandController extends Controller
{
    public function index()
	{
		$brands = Brand::all();
		return view('adminview/brands/index', ['brands' => $brands]);
	}

    public function create()
    {
    	return view('adminview/brands/create');
    }

    public function store(Request $request)
    {
    	$messages = [
    		'name.required' => 'Need to fill this field!',
    	];
    	$validator = Validator::make($request->all(), [
            'name' => 'required|unique:brands',
        ], $messages)->validate();
    	Brand::create($request->all());
    	return redirect('/admin/brands')
    	->with('message', 'Your brands is successfully created!')
    	->with('status', 'success');
    }

    public function edit($id)
    {
    	$brand = Brand::find($id);
    	return view('/adminview/brands/edit', [ 'brand' => $brand ]);
    }

    public function update(Request $request, $id)
    {
    	$brand = Brand::find($id);
    	$brand->update($request->all());
    	return redirect('/admin/brands')
    	->with('message', 'Your brands is successfully updated!')
    	->with('status', 'warning');
    }

    public function delete($id)
    {
    	$brand = Brand::find($id);
    	$brand->delete();

    	return redirect('/admin/brands')
    	->with('message', 'Your brands is successfully deleted!')
    	->with('status', 'danger');
    }
}

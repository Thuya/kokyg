<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;
use App\Color;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->get();
        return view('adminview/products/index', ['products' => $products]);
    }

    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        $colors = Color::all();
        return view('adminview/products/create', [
            'categories' => $categories,
            'brands' => $brands,
            'colors' => $colors
        ]);
    }

    public function store(ProductRequest $request)
    {
        Product::create($request->all());
        return redirect('/admin/products')
        ->with('message', 'Your product is successfully created!')
        ->with('status', 'success');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        $brands = Brand::all();
        $colors = Color::all();
        return view('/adminview/products/edit', [ 
            'product' => $product,
            'categories' => $categories,
            'brands' => $brands,
            'colors' => $colors 
        ]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update($request->all());
        return redirect('/admin/products')
        ->with('message', 'Your product is successfully updated!')
        ->with('status', 'warning');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('/adminview/products/show', [ 'product' => $product ]);
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('/admin/products')
        ->with('message', 'Your product is successfully deleted!')
        ->with('status', 'danger');
    }

    public function phone($id)
    {
    	$phones = Product::with('category')->where('category_id', $id)->get();
        $category = Category::find($id);
    	return view('userview/phone', [ 'phones' => $phones, 'category' => $category ]);
    }

    public function allproducts()
    {
        $phones = Product::all();
        
        return view('userview/phone', [ 'phones' => $phones ]);
    }

    public function detail($id)
    {
        $product = Product::find($id);
        return view('userview/detail', ['product' => $product]);
    }

    public function adminsearch(Request $request)
    {
        $product_search = $request->search;
        $products = Product::where('name', 'LIKE', '%' . $product_search . '%')->get();

        return view('/adminview/products/index', [ 'products' => $products ]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = ['name', 'price', 'totalqty', 'description', 'photo', 'category_id', 'cpu', 'ram', 'screen_display', 'resolution', 'storage', 'operating_system', 'camera', 'color_id', 'brand_id'];
   
   public function color()
   {
   		return $this->belongsTo('App\Color');
   }
   public function category()
   {
   		return $this->belongsTo('App\Category');
   }
   public function brand()
   {
   		return $this->belongsTo('App\Brand');
   }
   public function orderdetails()
   {
    return $this->hasMany('App\OrderDetail');
   }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    protected $fillable = [
    	'order_id',
		'product_id',
		'item_name',
		'quantity',
		'price',
    ];

    public function order()
    {
    	return $this->belongsTo('App\Order');
    }
    public function product()
    {
    	return $this->belongsTo('App\Product');
    }
}

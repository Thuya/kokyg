<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
    	'user_id',
		'payment',
		'note',
        'order_date',
        'order_month',
		'total_quantity',
		'total_amount',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function orderdetails()
    {
        return $this->HasMany('App\OrderDetail');
    }
}

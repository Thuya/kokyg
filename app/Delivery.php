<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = "deliveries";
    protected $fillable = [
    	'fees',
    	'date',
    	'contact_person',
    	'phone',
    	'address',
    	'status',
    	'order_id'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 255)->nullable();
            $table->text("description")->nullable();
            $table->string("photo", 255)->nullable();
            $table->unsignedInteger("category_id");
            $table->unsignedInteger("color_id");
            $table->unsignedInteger("brand_id");
            $table->string("cpu")->nullable();
            $table->string("ram")->nullable();
            $table->string("screen_display")->nullable();
            $table->string("resolution")->nullable();
            $table->string("storage")->nullable();
            $table->string("operating_system")->nullable();
            $table->string("camera")->nullable();
            $table->decimal("price", 6, 2);
            $table->unsignedInteger("totalqty");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

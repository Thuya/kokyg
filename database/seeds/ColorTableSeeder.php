<?php

use Illuminate\Database\Seeder;
use App\Color;
class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::create([
        	'name' => 'red'
        ]);
        Color::create([
        	'name' => 'green'
        ]);
        Color::create([
        	'name' => 'black'
        ]);
        Color::create([
        	'name' => 'white'
        ]);
        Color::create([
        	'name' => 'pink'
        ]);
        Color::create([
        	'name' => 'purple'
        ]);
        Color::create([
        	'name' => 'silver'
        ]);
        Color::create([
        	'name' => 'yellow'
        ]);
        Color::create([
        	'name' => 'brown'
        ]);
        Color::create([
        	'name' => 'blue'
        ]);
    }
}

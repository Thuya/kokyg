<?php

use Illuminate\Database\Seeder;
use App\Brand;
class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create([
        	'name' => 'samsung'
        ]);
        Brand::create([
        	'name' => 'apple'
        ]);
        Brand::create([
        	'name' => 'Xioame'
        ]);
        Brand::create([
        	'name' => 'Hwawei'
        ]);
        Brand::create([
        	'name' => 'Acer'
        ]);
        Brand::create([
        	'name' => 'Lenovo'
        ]);
    }
}

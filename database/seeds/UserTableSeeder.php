<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Admin',
        	'email' => 'admin@gmail.com',
        	'password' => Hash::make('admin1234'),
        	'role_id' => 1
        ]);
        User::create([
        	'name' => 'Kygkyi',
        	'email' => 'kygkyi@gmail.com',
        	'password' => Hash::make('asdf1234'),
        	'role_id' => 2
        ]);
    }
}

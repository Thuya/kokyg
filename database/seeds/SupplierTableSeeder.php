<?php

use Illuminate\Database\Seeder;
use App\Supplier;

class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplier::create([
        	'name' => 'TMW Distribution'
        ]);
        Supplier::create([
        	'name' => 'TNL Distribution'
        ]);
        Supplier::create([
        	'name' => 'KKL Distribution'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'name' => 'laptop'
        ]);
        Category::create([
        	'name' => 'phone'
        ]);
        Category::create([
        	'name' => 'tablet'
        ]);
        Category::create([
        	'name' => 'ipad'
        ]);
        Category::create([ 
        	'name' => 'MP3'
        ]);
        Category::create([
        	'name' => 'TV'
        ]);
        Category::create([
        	'name' => 'Mac'
        ]);
        Category::create([
            'name' => 'Printer'
        ]);
        Category::create([
            'name' => 'Projector'
        ]);
    }
}
